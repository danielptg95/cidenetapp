﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace Cidenet_web.Database
{
    public partial class Database
    {
        public DbConnection CurrentConnection() {

            DbConnection DbConnection = new SqlConnection(ConnectionStringGateway());
            return DbConnection;

        }

        public string ConnectionStringGateway()
        {
            return ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["CurrentDatabase"]].ToString();
        }

    }
}
