﻿using Cidenet_web.Models;
using Insight.Database;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Cidenet_web.Database
{
    public interface Procedures : IDbConnection, IDbTransaction
    {

        [Sql("dbo.DB_employees_List", CommandType.StoredProcedure)]
        List<Employees> DB_employees_List();

        [Sql("dbo.DB_empleados_Get", CommandType.StoredProcedure)]
        List<Employees> DB_empleados_Get(int id);

        [Sql("dbo.DB_empleados_Edit", CommandType.StoredProcedure)]
        void DB_empleados_Edit(string primer_Nombre, string otros_Nombres, string primer_Apellido, string segundo_Apellido,
            int id_tipo_identificacion, string numero_identificacion, int id_pais, DateTime fecha_ingreso, string area, int estado,
            DateTime fecha_registro, int? id);

        [Sql("dbo.DB_empleados_delete", CommandType.StoredProcedure)]
        void DB_empleados_delete(int id);

        [Sql("dbo.DB_Listas_Load", CommandType.StoredProcedure)]
        List<Select> DB_Listas_Load();

        [Sql("dbo.DB_empleados_Buscar", CommandType.StoredProcedure)]
        List<Employees> DB_empleados_Buscar(string primer_Nombre, string otros_Nombres, string primer_Apellido, string segundo_Apellido, int id_pais,
            int id_tipo_identificacion, string numero_identificacion, string correo, int? estado);

    }
}
