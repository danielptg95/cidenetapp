﻿$(document).ready(function () {
    $("#content_alert").hide();
    //Cargar filtros de busqueda
    cargarFiltros();

    //limpiar modal de registro cuando se cierre
    $('.modal').on('hidden.bs.modal', function (e) {
        $(this).find('form').trigger('reset');
    });

    //Cargar combos de seleccion de modal de registro
    $.ajax({
        type: "GET",
        url: "/api/Values/cargarListas",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            cargarListas(data, "select_pais", "select_tipo_ident", "select_area");
        }, //End of AJAX Success function  
        failure: function (data) {
            alert(data.responseText);
        }, //End of AJAX failure function  
        error: function (data) {
            alert(data.responseText);
        } //End of AJAX error function  
    });

    $(".modal").on("show.bs.modal", function () {
        //Configurar Max y Min de los campos de fecha
        cargarFechas();
    });

    //Cargar lista de empleados
    cargarListaEmpleados();
});

function cargarFiltros() {
    $.ajax({
        type: "GET",
        url: "/api/Values/cargarListas",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            cargarListas(data, "filtro_pais", "filtro_tipo_identificacion", "");
        }, //End of AJAX Success function  
        failure: function (data) {
            alert(data.responseText);
        }, //End of AJAX failure function  
        error: function (data) {
            alert(data.responseText);
        } //End of AJAX error function  
    });
}

function filtrarLista() {
    var BusquedaEmpleado = '{\"primer_Nombre\": "' + $("#filtro_primer_nombre").val() + '",' +
        '\"otros_Nombres\": "' + $("#filtro_otros_nombres").val() + '", ' +
        '\"primer_Apellido\": "' + $("#filtro_primer_apellido").val() + '",' +
        '\"segundo_Apellido\": "' + $("#filtro_segundo_apellido").val() + '",' +
        '\"pais\": ' + $("#filtro_pais").val() + ',' +
        '\"tipo_identificacion\": ' + $("#filtro_tipo_identificacion").val() + ',' +
        '\"numero_identificacion\": "' + $("#filtro_numero_identificacion").val() + '",' +
        '\"correo\": "' + $("#filtro_correo").val() + '"';

    BusquedaEmpleado = BusquedaEmpleado + ( $("#filtro_estado").val() == "" ? ' }' : (', \"estado\": ' + $("#filtro_estado").val() + ' }') );

    $.ajax({
        type: "POST",
        url: "/api/Values/busquedaEmpleado",
        data: BusquedaEmpleado,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {

            $('#tb_empleados').empty();

            $.each(data, function (i, item) {
                var rows = "<tr>" +
                    "<th scope=\"row\">" +
                    "<a id=\"edit_link\" href=\"#\" onclick=\"getEmpleado(" + item.id + ")\">Editar</a>" +
                    "</th>" +
                    "<th>" + item.primer_Nombre + "</th>" +
                    "<td>" + item.otros_Nombres + "</td>" +
                    "<td>" + item.primer_Apellido + "</td>" +
                    "<td>" + item.segundo_Apellido + "</td>" +
                    "<td>" + item.tipo_identificacion + "</td>" +
                    "<td>" + item.numero_identificacion + "</td>" +
                    "<td>" + item.pais + "</td>" +
                    "<td>" + item.correo + "</td>" +
                    "<td>" + (item.estado == 1 ? "Activo" : "Inactivo") + "</td>" +
                    "<td scope=\"row\">" +
                    "<a id=\"edit_link\" href=\"#\" onclick=\"deleteEmpleado(" + item.id + ")\">Eliminar</a>" +
                    "</td>" +
                    "</tr>";
                $('#tb_empleados').append(rows);
            }); //End of foreach Loop 
        },
        failure: function (response) {
            alert(response.responseText);
        },
        error: function (response) {
            alert(response.responseText);
        }
    });

}

function cargarFechas() {
    var now = new Date();
    var dt_fecha_Mes_anterior = now;
    var dt_fecha_actual = formatoFecha(now);
    now.setMinutes(now.getMinutes() - now.getTimezoneOffset());
    $("#input_fecha_registro").val(now.toISOString().slice(0, 16));

    dt_fecha_Mes_anterior.setMonth(dt_fecha_Mes_anterior.getMonth() - 1);
    dt_fecha_Mes_anterior = formatoFecha(dt_fecha_Mes_anterior);

    $("#input_fecha_ingreso").attr("max", dt_fecha_actual);
    $("#input_fecha_ingreso").attr("min", dt_fecha_Mes_anterior);
    
}

function cargarListaEmpleados() {
    $('#tb_empleados').empty();

    $.ajax({
        type: "GET",
        url: "/api/Values",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            $.each(data, function (i, item) {
                var rows = "<tr>" +
                    "<th scope=\"row\">" +
                    "<a id=\"edit_link\" href=\"#\" onclick=\"getEmpleado(" + item.id + ")\">Editar</a>" +
                    "</th>" +
                    "<th>" + item.primer_Nombre + "</th>" +
                    "<td>" + item.otros_Nombres + "</td>" +
                    "<td>" + item.primer_Apellido + "</td>" +
                    "<td>" + item.segundo_Apellido + "</td>" +
                    "<td>" + item.tipo_identificacion + "</td>" +
                    "<td>" + item.numero_identificacion + "</td>" +
                    "<td>" + item.pais + "</td>" +
                    "<td>" + item.correo + "</td>" +
                    "<td>" + (item.estado == 1 ? "Activo" : "Inactivo") + "</td>" +
                    "<td scope=\"row\">" +
                    "<a id=\"edit_link\" href=\"#\" onclick=\"deleteEmpleado(" + item.id + ")\">Eliminar</a>" +
                    "</td>" +
                    "</tr>";
                $('#tb_empleados').append(rows);
            }); //End of foreach Loop   
            console.log(data);
        }, //End of AJAX Success function  

        failure: function (data) {
            alert(data.responseText);
        }, //End of AJAX failure function  
        error: function (data) {
            alert(data.responseText);
        } //End of AJAX error function  
    });
}

function getEmpleado(id) {
    $.ajax({
        type: "GET",
        url: "/api/Values/" + id,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            $("#registerModal").modal('show');

            $.each(data, function (i, item) {
                $("#hidden_id_empleado").val(item.id == null ? 0 : item.id);
                $("#input_primer_apellido").val(item.primer_Apellido);
                $("#input_segundo_apellido").val(item.segundo_Apellido);
                $("#input_primer_nombre").val(item.primer_Nombre);
                $("#input_otros_nombres").val(item.otros_Nombres);
                $($('#select_pais').find('option:contains("' + item.pais + '")')[0]).prop('selected', true);
                $($('#select_tipo_ident').find('option:contains("' + item.tipo_identificacion + '")')[0]).prop('selected', true);
                $("#input_num_ident").val(item.numero_identificacion);
                $("#input_correo").val(item.correo);
                $("#input_fecha_ingreso").val(formatoFecha(new Date(item.fecha_ingreso)));
                $($('#select_area').find('option:contains("' + item.area + '")')[0]).prop('selected', true);
                $("#input_fecha_registro").val(new Date(item.fecha_registro).toISOString().slice(0, 16));
            }); //End of foreach Loop   
            
            console.log(data);
        }, //End of AJAX Success function  

        failure: function (data) {
            alert(data.responseText);
        }, //End of AJAX failure function  
        error: function (data) {
            alert(data.responseText);
        } //End of AJAX error function  
    });
}

function guardar_Empleado() {
    var _valid = true;

    //Validar campos requeridos
    $(".required").each(function () {
        if ($.trim($(this).val()) == "") {
            let campo = $("label[for=" + $(this).attr("id") + "]").html();
            alert("¡El dato " + campo + " es requerido!");
            _valid = false;
            $(this).focus();
            return false;
        }
    });

    if (!_valid) {
        return false;
    }

    //Validar expresiones regulares
    if (!validarFormulario()) {
        return false;
    }

    var empleado = '{\"id\": ' + $("#hidden_id_empleado").val() + ',' +
        '\"primer_Nombre\": "' + $("#input_primer_nombre").val() + '",'+
        '\"otros_Nombres\": "' + $("#input_otros_nombres").val() + '", '+
        '\"primer_Apellido\": "' + $("#input_primer_apellido").val() + '",'+
        '\"segundo_Apellido\": "' + $("#input_segundo_apellido").val() + '",' +
        '\"pais\": ' + $("#select_pais").val() + ',' +
        '\"tipo_identificacion\": ' + $("#select_tipo_ident").val() + ',' +
        '\"numero_identificacion\": "' + $("#input_num_ident").val() + '",' +
        '\"fecha_ingreso\": "' + $("#input_fecha_ingreso").val() + '",' +
        '\"area\": "' + $("#select_area").val() + '",' +
        '\"estado\": ' + ( $("#chk_estado").is(':checked') ? 1 : 0 ) + ',' +
        '\"fecha_registro\": "' + $("#input_fecha_ingreso").val() + '" }';

    $.ajax({
        type: "POST",
        url: "/api/Values",
        data: empleado,
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            alert("¡Se ha guardado la información del empleado!");
            cargarListaEmpleados();
        },
        failure: function (response) {
            alert(response.responseText);
        },
        error: function (response) {
            alert(response.responseText);
        }
    });
}

function deleteEmpleado(id) {
    $.ajax({
        type: "DELETE",
        url: "/api/Values/" + id,
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            alert("Empleado eliminado");
            cargarListaEmpleados();
        }, //End of AJAX Success function  

        failure: function (data) {
            alert(data.responseText);
        }, //End of AJAX failure function  
        error: function (data) {
            alert(data.responseText);
        } //End of AJAX error function  
    });
}