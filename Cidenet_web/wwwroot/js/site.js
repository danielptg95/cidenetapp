﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.


function validarFormulario() {
    var result = true;
    $("[exp_reg]").each(function () {
        var val = $(this).val();
        var exp = $(this).attr("exp_reg");

        if (new RegExp(/^\//).test(exp)) {
            exp = exp.substring(1);
        }
        if (new RegExp(/\/$/).test(exp)) {
            exp = exp.slice(0, -1);
        }

        if (!validarExpresionRegular(val, exp, $(this))) {
            alert("Valor para el campo " + " \"" + $(this).attr("title") + "\" " + " no es válido");
            $(this).focus();
            result = false;
            return false;
        }
    });

    return result;
}

function validarExpresionRegular(value, textExpr, obj) {
    var regex = new RegExp(textExpr);
    var result = value.match(regex);
    return !(result == null);
}

function formatoFecha(today) {
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();

    today = yyyy + '-' + mm + '-' + dd;
    return today;
}

function cargarListas(data, id_select_pais, id_select_tipo_ident, id_select_area) {
    $.each(data, function (i, item) {
        let id = "";
        switch (item.cd_type) {
            case "pais":
                id = id_select_pais;
                break;
            case "tipo_identificacion":
                id = id_select_tipo_ident;
                break;
            case "area":
                id = id_select_area;
                break;
        }

        if (id != "") {
            $('#' + id).append($('<option>', {
                value: item.id,
                text: item.ds_value
            }));
        }

        if (item.cd_type == "pais") {
            $('#' + id + ' option[value=' + item.id + ']').attr("cd", item.attr);
        }

    }); //End of foreach Loop 
}