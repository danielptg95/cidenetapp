If exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[DB_empleados_Buscar]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
Drop Procedure [dbo].DB_empleados_Buscar
GO
CREATE PROCEDURE dbo.DB_empleados_Buscar
		@primer_Nombre	VARCHAR(20),
		@otros_Nombres	VARCHAR(50),
		@primer_Apellido	VARCHAR(20),
		@segundo_Apellido	VARCHAR(20),
		@id_pais	int,
		@id_tipo_identificacion int,
		@numero_identificacion varchar(20),
		@correo varchar(300),
		@estado int NULL
WITH ENCRYPTION
AS
BEGIN
	Declare @ds_error varchar(max)

	BEGIN TRY

		Select Empleados.id, primer_Nombre, primer_Apellido, segundo_Apellido, otros_Nombres, Pais.pais as pais
		, Tipo_Identificacion.tipo_identificacion as tipo_identificacion, numero_identificacion
		, correo, fecha_ingreso, area, estado, fecha_registro 
		from dbo.Empleados
		Inner Join dbo.Pais ON Empleados.id_pais = Pais.id
		Inner Join dbo.Tipo_Identificacion On Empleados.id_tipo_identificacion = Tipo_Identificacion.id
		where Empleados.primer_Nombre = case when ISNULL(@primer_Nombre,'') <> '' then @primer_Nombre else Empleados.primer_Nombre end
		And Empleados.otros_Nombres = case when ISNULL(@otros_Nombres,'') <> '' then @otros_Nombres else Empleados.otros_Nombres end
		And Empleados.primer_Apellido = case when ISNULL(@primer_Apellido,'') <> '' then @primer_Apellido else Empleados.primer_Apellido end
		And Empleados.segundo_Apellido = case when ISNULL(@segundo_Apellido,'') <> '' then @segundo_Apellido else Empleados.segundo_Apellido end
		And Empleados.id_pais = case when ISNULL(@id_pais,0) <> 0 then @id_pais else Empleados.id_pais end
		And Empleados.id_tipo_identificacion = case when ISNULL(@id_tipo_identificacion,0) <> 0 then @id_tipo_identificacion else Empleados.id_tipo_identificacion end
		And Empleados.numero_identificacion = case when ISNULL(@numero_identificacion,'') <> '' then @numero_identificacion else Empleados.numero_identificacion end
		And Empleados.correo = case when ISNULL(@correo,'') <> '' then @correo else Empleados.correo end
		And Empleados.estado = case when @estado IS NOT NULL then @estado else Empleados.estado end

	END TRY
	Begin Catch

			exec [spLogError]
			Set @ds_error = ERROR_MESSAGE() 
			RaisError(@ds_error,16,1)
			Return  
	End Catch
END
GO
