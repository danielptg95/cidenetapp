If Exists (Select 1 From dbo.sysobjects Where id = object_id(N'[dbo].[FN_generarCorreo]') And type='FN')
	Drop Function [dbo].FN_generarCorreo
GO
CREATE FUNCTION dbo.FN_generarCorreo(@primer_Nombre varchar(10), @primer_Apellido varchar(10), @id_pais int)
RETURNS varchar(300) WITH ENCRYPTION
AS 
BEGIN
	------------------------------------------------------------------------------
	-- Variables
	------------------------------------------------------------------------------
	Declare @nombre_correo varchar(300), @dominio_correo varchar(15), @pais_correo varchar(2), @correo varchar(300), @id_correo int
	------------------------------------------------------------------------------
	-- operacion
	------------------------------------------------------------------------------

	--validacion correo
	Set @dominio_correo = '@cidenet.com.'
	Set @nombre_correo = LOWER(@primer_Nombre) + '.' + LOWER(Replace(@primer_Apellido,' ',''))
	Select @pais_correo = codigo from Pais where id = @id_pais

	Set @correo = @nombre_correo + @dominio_correo + @pais_correo

	--Si existe el correo
	IF exists(Select 1 from Empleados where correo = @correo)
	Begin
		--tomar el ultimo de id de la tabla
		Select @id_correo = ident_current('dbo.Empleados')
		Set @id_correo += 1

		--Se le agrega denominacion
		Set @correo = @nombre_correo + '.' + Convert(varchar,@id_correo) + @dominio_correo + @pais_correo
	End

	------------------------------------------------------------------------------
	-- return
	------------------------------------------------------------------------------
	return	 @correo
END
