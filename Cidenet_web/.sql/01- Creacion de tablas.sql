--tipos de identificacion
IF NOT EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.Tipo_Identificacion') AND Type = N'U')
BEGIN
	CREATE TABLE dbo.Tipo_Identificacion(
		id				INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
		tipo_identificacion	VARCHAR(50) NOT NULL
	)
END

IF EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.Tipo_Identificacion') AND Type = N'U')
BEGIN
	INSERT INTO dbo.Tipo_Identificacion (tipo_identificacion)
	VALUES ('C�dula de ciudadania'), ('C�dula de extranjer�a'), ('Pasaporte'), ('Permiso especial')
END
GO

--paises
IF NOT EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.Pais') AND Type = N'U')
BEGIN
	CREATE TABLE dbo.Pais(
		id			INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
		Pais		VARCHAR(20) NOT NULL,
		codigo		VARCHAR(2) NOT NULL
	)
END

IF EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.Pais') AND Type = N'U')
BEGIN
	INSERT INTO dbo.Pais (Pais,codigo)
	VALUES ('Colombia','co'), ('Estados Unidos','us')
END
GO

--area
IF NOT EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.Area') AND Type = N'U')
BEGIN
	CREATE TABLE dbo.Area(
		id			INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
		area		VARCHAR(50) NOT NULL
	)
END

IF EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.Area') AND Type = N'U')
BEGIN
	INSERT INTO dbo.Area (Area)
	VALUES ('Administraci�n'), ('Financiera'), ('Financiera'), ('Compras'), ('Infraestructura'), ('Operaci�n'), ('Talento Humano'), ('Servicios Varios')
END
GO

--Empleados
IF NOT EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.Empleados') AND Type = N'U')
BEGIN
	CREATE TABLE dbo.Empleados(
		id				INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
		primer_Nombre	VARCHAR(20) NOT NULL,
		primer_Apellido	VARCHAR(20) NOT NULL,
		segundo_Apellido	VARCHAR(20) NOT NULL,
		otros_Nombres	VARCHAR(50) NULL,
		id_pais	int NOT NULL,
		id_tipo_identificacion int NOT NULL,
		numero_identificacion varchar(20) NOT NULL,
		correo varchar(300) NOT NULL,
		fecha_ingreso datetime NOT NULL,
		area varchar(50),
		estado int default 1,
		fecha_registro datetime default getdate(),
		fecha_edicion datetime default getdate()
	)    
END
GO

--Tabla log de errores
IF NOT EXISTS(SELECT 1 FROM sys.Objects WHERE  Object_id = OBJECT_ID(N'dbo.Error_log') AND Type = N'U')
BEGIN
	CREATE TABLE dbo.Error_log(
		id				INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
		codigo			nvarchar(50) NOT NULL,
		mensaje			nvarchar(4000) NULL,
		procedimiento	nvarchar(100) NULL,
		linea		 	int NULL,
		fecha			datetime default getdate()
	)    
END
GO
