If exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[spLogError]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
Drop Procedure [dbo].[spLogError]
GO
CREATE PROCEDURE [dbo].[spLogError] 
AS
BEGIN
    SET NOCOUNT ON;
    BEGIN TRY
        -- Return si no hay informacion de error.
        IF ERROR_NUMBER() IS NULL
        RETURN;

        -- Return si se encuentra una transaccion activa que no se ha hecho rollback.
        IF XACT_STATE() = -1
        BEGIN
            PRINT 'No se puede registrar el error porque la transacción actual se encuentra activa. '
            RETURN;
        END;

        INSERT [dbo].[Error_log] ( codigo ,mensaje ,procedimiento ,linea, fecha) 
        VALUES 
            (
            ERROR_NUMBER(),
            ERROR_MESSAGE(),
            ERROR_PROCEDURE(),
            ERROR_LINE(),
            CURRENT_TIMESTAMP
            );

    END TRY
    BEGIN CATCH
		PRINT 'An error occurred in stored procedure uspLogError: ';
        RETURN -1;
    END CATCH
END