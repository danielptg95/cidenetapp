If exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[DB_empleados_Edit]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
Drop Procedure [dbo].DB_empleados_Edit
GO
CREATE PROCEDURE dbo.DB_empleados_Edit
		@id				INT NULL,
		@primer_Nombre	VARCHAR(20),
		@primer_Apellido	VARCHAR(20),
		@segundo_Apellido	VARCHAR(20),
		@otros_Nombres	VARCHAR(50),
		@id_pais	int,
		@id_tipo_identificacion int,
		@numero_identificacion varchar(20),
		@fecha_ingreso datetime,
		@area varchar(50),
		@estado int,
		@fecha_registro datetime
WITH ENCRYPTION
AS
BEGIN
	Declare @ds_error varchar(max), @correo varchar(300)

	BEGIN TRY

		IF ISNULL(@id,0) = 0
		BEGIN
			--Se genera correo del empleado
			Select @correo = dbo.FN_generarCorreo(@primer_Nombre, @primer_Apellido, @id_pais)

			INSERT INTO dbo.Empleados(primer_Nombre, primer_Apellido, segundo_Apellido, otros_Nombres, id_pais, id_tipo_identificacion,
			numero_identificacion, correo, fecha_ingreso, area, estado)
			VALUES(@primer_Nombre, @primer_Apellido, @segundo_Apellido, @otros_Nombres, @id_pais, @id_tipo_identificacion, @numero_identificacion,
			@correo, @fecha_ingreso, @area, @estado)
		END
		ELSE
		BEGIN
			update dbo.Empleados
			set primer_Nombre = @primer_Nombre,
				primer_Apellido = @primer_Apellido,
				segundo_Apellido = @segundo_Apellido,
				otros_Nombres = @otros_Nombres,
				id_pais = @id_pais,
				id_tipo_identificacion = @id_tipo_identificacion,
				numero_identificacion = @numero_identificacion,
				correo = @correo,
				fecha_ingreso = @fecha_ingreso,
				area = @area,
				estado = @estado,
				fecha_edicion = getdate()
			where id = @id
		END

	END TRY
	Begin Catch
			exec [spLogError]
			Set @ds_error = ERROR_MESSAGE() 
			RaisError(@ds_error,16,1)
			Return  
	End Catch
END
GO
