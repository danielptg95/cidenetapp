If exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[DB_Listas_Load]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
Drop Procedure [dbo].DB_Listas_Load
GO
CREATE PROCEDURE dbo.DB_Listas_Load
WITH ENCRYPTION
AS
BEGIN
	Declare @tbl_temp table(id_record int identity(1,1), cd_type varchar(30), id int, ds_value varchar(50), attr varchar(2) default '')
	Declare @ds_error varchar(max)

	BEGIN TRY

		Insert into @tbl_temp(cd_type, id, ds_value, attr)
		Select 'pais', id, Pais, codigo from dbo.Pais

		Insert into @tbl_temp(cd_type, id, ds_value)
		Select 'tipo_identificacion', id, tipo_identificacion from dbo.Tipo_Identificacion

		Insert into @tbl_temp(cd_type, id, ds_value)
		Select 'area', id, Area from dbo.Area

		Select cd_type, id, ds_value, attr from @tbl_temp

	END TRY
	Begin Catch
			exec [spLogError]
			Set @ds_error = ERROR_MESSAGE() 
			RaisError(@ds_error,16,1)
			Return  
	End Catch
END
GO
