If exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[DB_empleados_Get]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
Drop Procedure [dbo].DB_empleados_Get
GO
CREATE PROCEDURE dbo.DB_empleados_Get
	@id int
WITH ENCRYPTION
AS
BEGIN
		
		Select Empleados.id, primer_Nombre, primer_Apellido, segundo_Apellido, otros_Nombres, Pais.pais as pais
		, Tipo_Identificacion.tipo_identificacion as tipo_identificacion, numero_identificacion
		, correo, fecha_ingreso, Area.area, estado, fecha_registro 
		from dbo.Empleados
		Inner Join dbo.Pais ON Empleados.id_pais = Pais.id
		Inner Join dbo.Tipo_Identificacion On Empleados.id_tipo_identificacion = Tipo_Identificacion.id
		Inner Join dbo.Area ON Empleados.area = Area.id
		where Empleados.id = @id
		
END
GO
