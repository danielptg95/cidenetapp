If exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[DB_empleados_Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
Drop Procedure [dbo].DB_empleados_Delete
GO
CREATE PROCEDURE dbo.DB_empleados_Delete
	@id int
WITH ENCRYPTION
AS
BEGIN
	Declare @ds_error varchar(max)

	BEGIN TRY
		
		Delete from dbo.Empleados where Empleados.id = @id

	END TRY
	Begin Catch
			exec [spLogError]
			Set @ds_error = ERROR_MESSAGE() 
			RaisError(@ds_error,16,1)
			Return  
	End Catch
		
END
GO
