﻿using Cidenet_web.Database;
using Cidenet_web.Models;
using Insight.Database;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Cidenet_web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private Procedures _repositorioProc;


        public ValuesController()
        {
            Cidenet_web.Database.Database obj = new Cidenet_web.Database.Database();
            _repositorioProc = obj.CurrentConnection().As<Procedures>();
        }

        // GET: api/<ValuesController>
        //Obtener lista de empleados
        [HttpGet]
        public IEnumerable<Employees> Get()
        {
            var result = _repositorioProc.DB_employees_List();
            return result;
        }

        // GET api/<ValuesController>/id
        //Obtener información de un empleado
        [HttpGet("{id}")]
        public IEnumerable<Employees> Get(int id)
        {
            var result = _repositorioProc.DB_empleados_Get(id);
            return result;
        }

        // GET api/<ValuesController>/cargarListas
        //Cargar listas
        [HttpGet("cargarListas")]
        public IEnumerable<Select> cargarListas()
        {
            var result = _repositorioProc.DB_Listas_Load();
            return result;
        }

        // POST api/<ValuesController>
        //Guardar y actualizar información de empleado
        [HttpPost]
        public void Post(Employees employees)
        {
            try
            {
                _repositorioProc.DB_empleados_Edit(employees.primer_Nombre, employees.otros_Nombres, employees.primer_Apellido, employees.segundo_Apellido,
                Convert.ToInt32(employees.tipo_identificacion.ToString()), employees.numero_identificacion, Convert.ToInt32(employees.pais.ToString()), employees.fecha_ingreso, employees.area,
                employees.estado, employees.fecha_registro, employees.id);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        // POST api/<ValuesController>/busquedaEmpleado
        //Buscar empleados
        [HttpPost("busquedaEmpleado")]
        public IEnumerable<Employees> busquedaEmpleado(BuscarEmpleado buscarEmpleado)
        {
            try
            {
                var result = _repositorioProc.DB_empleados_Buscar(buscarEmpleado.primer_Nombre, buscarEmpleado.otros_Nombres, buscarEmpleado.primer_Apellido,
                buscarEmpleado.segundo_Apellido, buscarEmpleado.pais, buscarEmpleado.tipo_identificacion, buscarEmpleado.numero_identificacion,
                buscarEmpleado.correo, buscarEmpleado.estado);
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        // DELETE api/<ValuesController>/id
        //Eliminar Empleado
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            try
            {
                _repositorioProc.DB_empleados_delete(id);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
