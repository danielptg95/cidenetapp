﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cidenet_web.Models
{
    public class Select
    {
        public int id { get; set; }
        public string cd_type { get; set; }
        public string ds_value { get; set; }
        public string attr { get; set; }

    }
}
