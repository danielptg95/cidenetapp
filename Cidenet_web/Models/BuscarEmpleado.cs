﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cidenet_web.Models
{
    public class BuscarEmpleado
    {
        public string primer_Nombre { get; set; }
        public string otros_Nombres { get; set; }
        public string primer_Apellido { get; set; }
        public string segundo_Apellido { get; set; }
        public int pais { get; set; }
        public int tipo_identificacion { get; set; }
        public string numero_identificacion { get; set; }
        public string correo { get; set; }
        public int? estado { get; set; }
    }
}
