﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cidenet_web.Models
{
    public class Employees
    {
        public int? id { get; set; }
        public string primer_Nombre { get; set; }
        public string otros_Nombres { get; set; }
        public string primer_Apellido { get; set; }
        public string segundo_Apellido { get; set; }
        public object pais { get; set; }
        public object tipo_identificacion { get; set; }
        public string numero_identificacion { get; set; }
        public string? correo { get; set; }
        public DateTime fecha_ingreso { get; set; }
        public string area { get; set; }
        public int estado { get; set; }
        public DateTime fecha_registro { get; set; }
    }
}
